<?php

require '../../vendor/autoload.php';
require('func.php');

/*print_r($_FILES);*/

$fileType = $_FILES['buyerWallet']['type'];
$uploadfile = $_FILES['buyerWallet']['name'];

$res = move_uploaded_file($_FILES['buyerWallet']['tmp_name'], $uploadfile);

$resizedfile = 'resized__' . $uploadfile;
makeThumbnail($uploadfile, 800, 800, $resizedfile, $fileType);
$uploadfile = $resizedfile;

$qrcode = new QrReader($uploadfile);
$text = $qrcode->text(); //return decoded text from QR Code


echo json_encode(['decoded_text' => $text]);


?>